/*global define*/

define([
    'underscore',
    'backbone',
    'collections/ItemsCollection',
    'views/ThreadView'
], function (_, Backbone, ItemsCollection, ThreadView) {
    'use strict';

    var ThreadModel = Backbone.Model.extend({
        defaults: {
        },
        initialize: function(model, data) {

            var matchingItems,
                self = this,
                title = model.title;

            this.view = new ThreadView({model: this});
        	matchingItems = _(_(data).filter(function(item) { return item.threadID === title})).map(function(item) { return _(item).extend({$wrapper: self.view})});
            this.items = new ItemsCollection(matchingItems);

        }
    });

    return ThreadModel;
});
