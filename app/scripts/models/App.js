/*global define*/

define([
    'underscore',
    'backbone',
    'views/MainView',
    'views/SMPView',
    'collections/ThreadsCollection',
    'text!data/items.json',
    'events'
], function (_, Backbone, MainView, SMPView, ThreadsCollection, data, Events) {
    'use strict';

    var activeItem, activeThread,
        playPos = 0,
        ticker,
        defaultThread = 'LIVE',
        AppModel = Backbone.Model.extend({
        initialize: function() {
            this.view = new MainView();
            this.view.render();


            this.player = new SMPView();
            this.player.delegate = this;

            this.threads = new ThreadsCollection(false, JSON.parse(data).items);

            window.threads = this.threads;

            Events.bind('itemClicked', this.activateItem.bind(this));
            Events.bind('threadClicked', this.selectThread.bind(this));
            Events.bind('tick', this.checkCurrentTime.bind(this));
            Events.bind('tick', this.positionPlayHead.bind(this));
            Events.bind('forcePlayPos', this.setPlayPos.bind(this));


            //set default thread and start app
            this.selectThread(this.threads.findWhere({'title':defaultThread}));
        },
        activateItem: function(item, timeOffset) {
            try{
                this.stopTicker();
            var pid = item.get('PID'),
                startTime = item.get('startTime');
            activeItem = item;
            activeThread = this.threads.findWhere({title: item.get('threadID')});
            this.switchActiveThreadClass(activeThread);
            activeThread.items.activeIndex = activeItem.collection.indexOf(activeItem);

            $('a.item').removeClass('isActive');
            $('.item-' + item.cid).addClass('isActive');
            this.player.render(pid, startTime + (timeOffset || 0), activeItem);
            }
            catch(e){console.error("Reached end");}
        },
        switchActiveThreadClass: function(thread) {
            $('.thread-text').removeClass('isActive');
            $('.thread-' + thread.cid).addClass('isActive');
        },
        selectThread: function(thread) {
            this.stopTicker();
            var offset,
                item = thread.items.atPosition(playPos);

            this.switchActiveThreadClass(thread);
            offset = playPos - item.threadStartTime;
            this.activateItem(item, offset);
            activeThread = thread;
        },
        checkCurrentTime: function(playPos) {
            if (playPos >= activeItem.threadEndTime()) {
                this.playNextInCurrentThread();
            }
        },
        playNextInCurrentThread: function() {
            this.activateItem(activeThread.items.next());
        },
        stopTicker: function() {
            if (ticker) {
                clearInterval(ticker);
                ticker = null;
            }
        },
        startTicker: function() {
            if (ticker) {
                return false;
            }
            var subTicker = 0;
            ticker = setInterval(function() {
                subTicker += 1;
                if (subTicker >= 100) {
                    playPos++;
                    subTicker = 0;
                    Events.trigger('tick', playPos);
                }

            }, 10);
        },
        setPlayPos: function(pos) {
            playPos = pos || 0;
        },
        positionPlayHead: function() {
            var position,
                threadWidth = $('.thread-wrapper .items').width(),
                totalLength = 55,//this.threads.totalLength(),
                offset = 0;//$('.thread-wrapper').width() * .10;

            position = playPos / totalLength * threadWidth + offset - 50;
            $('.playhead').css({left: position + 'px'});
        }

    });

    return AppModel;
});
