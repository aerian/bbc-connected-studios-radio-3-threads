/*global define*/

define([
    'underscore',
    'backbone',
    'views/SMPView'
], function (_, Backbone, View) {
    'use strict';

    var PidmodelModel = Backbone.Model.extend({
        initialize: function() {
        	this.view = new View();
        }
    });

    return PidmodelModel;
});
