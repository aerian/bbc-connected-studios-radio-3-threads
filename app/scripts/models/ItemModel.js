/*global define*/

define([
    'underscore',
    'backbone',
    'views/ItemView'
], function(_, Backbone, ItemView) {
    'use strict';

    var ItemmodelModel = Backbone.Model.extend({
        defaults: {
        },
        initialize: function() {
            this.view = new ItemView({model: this});
        },
        threadStartTime: function() {
            var thread = this.collection,
                index = thread.indexOf(this),
                time = 0;

            for (var i = 0; i < index; i++) {
                time += thread.at(i).get('duration');
            }

            return time;
        },
        threadEndTime: function() {
            return this.threadStartTime() + this.get('duration');
        }
    });

    return ItemmodelModel;
});
