/*global define*/

define([
    'underscore',
    'backbone',
    'models/ThreadModel'
], function (_, Backbone, ThreadModel) {
    'use strict';

    var ThreadscollectionCollection = Backbone.Collection.extend({
        model: ThreadModel,
        initialize: function(model, data) {
            var self = this,
                uniqueThreads = _(_(data).map(function(elem) { return elem.threadID;})).uniq();

            _(uniqueThreads).each(function(title) {
                self.add(new ThreadModel({title: title}, data));
            });
        },
        totalLength: function() {
            return this.max(function(thread){ return thread.items.totalLength(); }).items.totalLength();
        },
        setActive: function(thread) {
            this.each(function(thread) {
                thread.set('isActive', "");
            });

            thread.set('isActive', 'isActive');
            $('.thread-wrapper').empty();
            this.each(function(thread) {
                thread.view.render();
            });

        }
    });

    return ThreadscollectionCollection;
});
