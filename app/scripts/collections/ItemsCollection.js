/*global define*/

define([
    'underscore',
    'backbone',
    'models/ItemModel'
], function (_, Backbone, ItemModel) {
    'use strict';

    var ItemsCollection = Backbone.Collection.extend({
        model: ItemModel,
        atPosition: function(pos) {
            if (typeof pos != 'number') { console.error('atPosition needs a number');}
            var currentOffset = 0,
                index = 0;

            while (currentOffset < pos && this.at(index)) {
                currentOffset += this.at(index).get('duration');
                if (currentOffset < pos) index++;
            }
            return this.at(index) || this.last();

        },
        next: function() {
            return this.at(this.activeIndex + 1 || 0);
        },
        totalLength: function() {
            var totalLength = this.reduce(function(memo, num) { return memo + num.get('duration'); }, 0);
            return totalLength;
        }
    });

    return ItemsCollection;
});
