/* 
 * Author: john.farrow
 * 
 */
define(['backbone'], function(Backbone) {
	
    return _.extend({}, Backbone.Events);  //global events channel
});