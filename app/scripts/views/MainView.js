define(['backbone', 'text!templates/main.html'], function(Backbone, Template) {
    var MainviewView = Backbone.View.extend({
        el: '#blq-content',
        render: function() {
            this.$el.html(Template);
            return this;
        }
    });

    return MainviewView;
});