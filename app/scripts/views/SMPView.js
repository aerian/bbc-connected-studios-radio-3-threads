/*global define*/

define([
    'bump-3',
    'underscore',
    'backbone',
    'text!templates/smp.html',
    'mustache'
], function($, _, Backbone, Template, mustache) {
    'use strict';

    var PidviewView = Backbone.View.extend({
        el: '#mediaPlayer',
        render: function(pid, startTime, item) {
            if (!pid) {
                console.error("Must include PID in SMPView.render");
            }
            this.createSMP(pid, startTime);

            $('.details').html(this.renderedTemplate(item));
        },
        renderedTemplate: function(item) {
            var data = item.toJSON();
            return mustache.render(Template, data);
        },
        createSMP: function(pid, startTime) {
            // this.$el.empty();
            var settings = {
                product: 'iplayer',
                playerProfile: 'smp',
                responsive: true,
                startTime: startTime || 0,
                autoplay: true
            };
            this.mediaPlayer = $('#media-player').player(settings);


            this.mediaPlayer.load('http://www.bbc.co.uk/iplayer/playlist/' + pid);
            this.mediaPlayer.bind('playing', this.delegate.startTicker.bind(this));
            this.mediaPlayer.bind('pause', this.delegate.stopTicker.bind(this));
        }
    });

    return PidviewView;
});