/*global define*/

define([
    'jquery',
    'underscore',
    'mustache',
    'backbone',
    'events',
    'text!templates/thread.html'
], function($, _, mustache, Backbone, Events, Template) {
    'use strict';

    var ThreadView = Backbone.View.extend({
        events: {
            'click a': 'threadClicked'
        },
        threadClicked: function(e) {
            e.preventDefault();
            Events.trigger('threadClicked', this.model);

        },
        getTemplate: function() {
            this.model.set('cid', this.model.cid);
            return mustache.render(Template, this.model.toJSON());
        },
        initialize: function() {
            var title = this.model.get('title').toLowerCase();
            this.render();
            this.$el.addClass(title + '-thread thread');
        },
        render: function() {
            $('.thread-wrapper').append(this.$el.html(this.getTemplate()));
        }
    });

    return ThreadView;
});