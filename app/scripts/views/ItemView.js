/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'mustache',
    'text!templates/item.html',
    'events'
], function($, _, Backbone, mustache, Template, Events) {
    'use strict';

    var ItemView = Backbone.View.extend({
        className: 'item',
        events: {
            'click a': 'itemClicked'
        },
        itemClicked: function(e) {
            e.preventDefault();
            e.stopPropagation();
            Events.trigger('itemClicked', this.model);
            Events.trigger('forcePlayPos', this.model.threadStartTime());
        },
        getTemplate: function() {
            var data = this.model.toJSON();
            data.threadItemTitle = this.concatTitle();
            data.cid = this.model.cid;
            return mustache.render(Template, data);
        },
        initialize: function() {
            this.$wrapper = this.model.get('$wrapper').$el;
            this.render();
        },
        render: function() {
            this.$wrapper.find('.items').append(this.$el.html(this.getTemplate()));
        },
        concatTitle: function() {
            var title = this.model.get('itemTitle'),
                newTitle = '';
            if (title.length < 30) return title;


            _(title.split('')).each(function(letter, i) {
                if (i < 26) {
                    newTitle += letter;
                }
            });

            return newTitle + '...';

        }
    });

    return ItemView;
});