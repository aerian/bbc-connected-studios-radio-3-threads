/*global require*/
'use strict';
require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        },
        mustache: {
            exports: 'mustache'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/underscore/underscore',
        bootstrap: '../bower_components/sass-bootstrap/dist/js/bootstrap',
        mustache: '../bower_components/mustache/mustache',
        text: '../bower_components/requirejs-text/text',
        'jquery-1.9': 'http://static.bbci.co.uk/frameworks/jquery/0.3.0/sharedmodules/jquery-1.9.1',
        'swfobject-2': 'http://static.bbci.co.uk/frameworks/swfobject/0.1.10/sharedmodules/swfobject-2',
        'bump-3': 'http://emp.bbci.co.uk/emp/bump-3/bump-3'
    },
    waitSeconds: 1
});

require([
    'backbone',
    'models/App' //,
    // 'mustache',
    // 'text!templates/ItemView.ejs'
], function(Backbone, App) {
    var App = new App();
    Backbone.history.start();
});