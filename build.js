({
    baseUrl: 'app/scripts',
    optimize: 'none',
    name: 'main',
    out: 'app/scripts/build/build.js',
    paths: {
        'jquery': '../../app/bower_components/jquery/jquery',
        'underscore': '../../app/bower_components/underscore/underscore',
        'backbone': '../../app/bower_components/backbone/backbone',
        'mustache': '../../app/bower_components/mustache/mustache',
        'text': '../../app/bower_components/requirejs-text/text',
        'bump-3': 'empty:',
        'jquery-1.9': 'empty:',
        'swfobject-2': 'empty:'
    },
    preserveLicenseComments: false,
})